package com.kozlov.SecondTask.Model;
//Invoker
public class User {
    Command first;
    Command second;
    Command third;
    Command fourth;
    public User (Command first, Command second, Command third, Command fourth){
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
    }
   public  void firstExample(){
        first.execute();
    }
    public void secondExample(){
        second.execute();
    }
    public void thirdExample(){
        third.execute();
    }
    public void fourthExample(){
        fourth.execute();
    }
}
