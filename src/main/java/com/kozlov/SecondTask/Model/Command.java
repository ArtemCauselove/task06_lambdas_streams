package com.kozlov.SecondTask.Model;

@FunctionalInterface
public interface Command{
    void execute();
}
