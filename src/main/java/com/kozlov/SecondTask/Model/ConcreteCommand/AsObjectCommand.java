package com.kozlov.SecondTask.Model.ConcreteCommand;

import com.kozlov.SecondTask.Model.Command;
import com.kozlov.SecondTask.Model.Item;

public class AsObjectCommand implements Command {
    Item item;
    public AsObjectCommand(Item item){
        this.item = item;
    }
    @Override
    public void execute(){
        item.asObject();
    }
}
