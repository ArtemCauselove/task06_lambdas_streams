package com.kozlov.SecondTask.Model.ConcreteCommand;

import com.kozlov.SecondTask.Model.Command;
import com.kozlov.SecondTask.Model.Item;

public class LambdaCommand implements Command {
    Item item;
    public LambdaCommand(Item item){
        this.item = item;
    }
    @Override
    public void execute(){
     item.lambda();
    }
}
