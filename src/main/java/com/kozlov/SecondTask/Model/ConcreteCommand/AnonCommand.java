package com.kozlov.SecondTask.Model.ConcreteCommand;

import com.kozlov.SecondTask.Model.Command;
import com.kozlov.SecondTask.Model.Item;

public class AnonCommand implements Command {
    Item item;
    public AnonCommand(Item item){
        this.item = item;
    }
    @Override
    public void execute(){
        item.anonClass();
    }
}
