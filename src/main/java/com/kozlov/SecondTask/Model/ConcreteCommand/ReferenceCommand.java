package com.kozlov.SecondTask.Model.ConcreteCommand;

import com.kozlov.SecondTask.Model.Command;
import com.kozlov.SecondTask.Model.Item;

public class ReferenceCommand implements Command {
    Item item;
    public ReferenceCommand(Item item){
        this.item = item;
    }
    @Override
    public void execute(){
        item.methodReference();
    }
}
