package com.kozlov.SecondTask.Model;

import com.kozlov.SecondTask.View.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//Reciever
public class Item {
    private static Logger logger = LogManager.getLogger(View.class);
    Command comand;
    addHelloToString Hello;
    String string;

    public Item(){}

    public Item(String string){
        this.string = string;
    }

     public void lambda(){
        Hello = ()->(string = "Lambda Hello " + string);
        logger.info(Hello.addHello());
        }

     public void methodReference(){
        Item item = new Item();
        comand = item::addNumber;
         logger.info(comand);
     }

     public void anonClass() {
         Runnable r = new Runnable() {
             @Override
             public void run() {
                 logger.info(string + " as anon class");
             }
         };
         r.run();
     }
     public void asObject(){
        Item currentItem = new Item("Hello ");
         logger.info(currentItem.string + "as Object class");
         }

    public String addNumber(){
        return (string + " " + 5);
    }
}