package com.kozlov.SecondTask;

import com.kozlov.SecondTask.Controller.Controller;

public class Application {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.start();
    }
}
