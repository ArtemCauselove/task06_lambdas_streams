package com.kozlov.SecondTask.View;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    Scanner input;
    private Map<String, String> menu;
    private static Logger logger = LogManager.getLogger(View.class);
   public void start(){
    logger.info("");
       input = new Scanner(System.in);
       menu = new LinkedHashMap<>();
       menu.put("1", "  1 - lambda");
       menu.put("2", "  2 - method reference");
       menu.put("3", "  3 - anon class");
       menu.put("4", "  4 - object of command class.");
       menu.put("5", "  Please type string");
   }
    public  void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
