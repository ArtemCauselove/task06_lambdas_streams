package com.kozlov.SecondTask.Controller;

import com.kozlov.SecondTask.Model.ConcreteCommand.AnonCommand;
import com.kozlov.SecondTask.Model.ConcreteCommand.AsObjectCommand;
import com.kozlov.SecondTask.Model.ConcreteCommand.LambdaCommand;
import com.kozlov.SecondTask.Model.ConcreteCommand.ReferenceCommand;
import com.kozlov.SecondTask.Model.Item;
import com.kozlov.SecondTask.Model.User;
import com.kozlov.SecondTask.View.View;

import java.util.Scanner;

public class Controller {
    Scanner scan;
    public void start(){
        View view = new View();
        view.start();
        view.outputMenu();
        scan = new Scanner(System.in);
        int number = scan.nextInt();
        String line = scan.next();
        Item firstItem = new Item(line);
        User user = new User(new LambdaCommand(firstItem), new ReferenceCommand(firstItem),
                new AnonCommand(firstItem), new AsObjectCommand(firstItem));
        switch (number){
            case 1: user.firstExample();
            break;
            case 2: user.secondExample();
            break;
            case 3: user.thirdExample();
            break;
            case 4: user.fourthExample();
            break;
        }
    }
}

