package com.kozlov.ThirdTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomArrays {
    private static Logger logger = LogManager.getLogger(RandomArrays.class);
    public List<Integer> randomArrayRandom(){
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        random.ints(10, 0, 10000).boxed().forEach(list::add);
        return list;
    }
    public List<Integer> randomArrayMathRandom(){
        List<Integer> randomIntegers;
        randomIntegers = Stream.generate(Math::random).map(n -> n * 10000)
                .mapToInt(Double::intValue)
                .limit(10)
                .boxed()
                .collect(Collectors.toCollection(ArrayList::new));
        return randomIntegers;
    }
    public void start(){
        List<Integer> firstList = randomArrayMathRandom();
        List<Integer> secondList = randomArrayRandom();
        firstList.forEach(s->logger.info(s + " - "));
        firstList.stream()
                .max(Comparator.comparing(i -> i))
                .ifPresent(max -> logger.info("\nMaximum is " + max));
        firstList.stream()
                .min(Comparator.comparing(i -> i))
                .ifPresent(min -> logger.info("\nMinimum is " + min));
        int sum1 = firstList.stream()
                .mapToInt(i -> i)
                .sum();
        logger.info("\nSum  is " + sum1);
        Optional sum2 = firstList.stream().reduce((i, j) -> i+j);
        logger.info("\nSum  is " + sum2.get());
        OptionalDouble average = firstList.stream()
                .mapToInt(i -> i)
                .average();
        logger.info("\nAverage is " + average.getAsDouble() + "\nBigger than average ");
        logger.info(" " + firstList.stream().filter(n -> n > average.getAsDouble() ).count());
    }
}
