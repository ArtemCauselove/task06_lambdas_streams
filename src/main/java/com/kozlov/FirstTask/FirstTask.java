package com.kozlov.FirstTask;

import com.kozlov.SecondTask.View.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FirstTask {
    public static void main(String[] args) {
         Logger logger = LogManager.getLogger(FirstTask.class);
        Calculatable average = (a,b,c)->((a+b+c)/3);
        Calculatable max = (a,b,c)->(Math.max(a, Math.max(b,c)));
        logger.info(max.count(10,2,5) + "   " + average.count(2,30,4));
    }
}
@FunctionalInterface
 interface Calculatable{
int count (int a, int b, int c);
}
