package com.kozlov.FourthTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StringSort {
    private static Logger logger = LogManager.getLogger(StringSort.class);
    List<String> listOfLines;
    public void start(){
    askForPrint();
    sortedElements();
    numberOfUnique();
    Occurance();
    OccuranseLowerCase();
    }
    public void askForPrint() {
        listOfLines = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String line;
        System.out.println("\nPlease insert text:");
        while (!(line = sc.nextLine()).equals("")) {
            listOfLines.add(line);
        }
    }
        void sortedElements() {
            logger.info("\nList of sorted unique elements in the stream: "
                    + listOfLines.stream()
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList()));
        }
        void numberOfUnique() {
            logger.info("\nNumber of unique words : " + listOfLines.stream().distinct().count());
        }
        void Occurance(){
            logger.info("\nOccurrence number of each word in the text: " + listOfLines.stream()
                    .collect(Collectors.groupingBy(a -> a, Collectors.counting())));
        }
        void OccuranseLowerCase(){
            logger.info("\nOccurrence number of each word (except Uppercase) in the text: " + listOfLines.stream()
                    .filter(str -> Character.isLowerCase(str.charAt(0)))
                    .collect(Collectors.groupingBy(a -> a, Collectors.counting())));
        }
        void OccuranseSymbols() {
        listOfLines.stream().collect(Collectors.groupingBy(a -> a, Collectors.counting() ));
        }
    }
